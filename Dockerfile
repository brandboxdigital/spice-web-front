# Build
# FROM node:14-alpine as builder
# WORKDIR /app
# RUN apk --no-cache add openssh g++ make python3 git
# COPY package.json /app/
# RUN yarn install && yarn cache clean --force
# ADD . /app
# RUN yarn build

# Run
# FROM node:14-alpine
# FROM --platform=linux/arm64 arm64v8/node:22-alpine
FROM --platform=linux/arm64 arm64v8/node:22-slim
WORKDIR /app
# COPY --from=builder /app /app
COPY . /app
EXPOSE 3000

# CMD [ "node", ".output/server/index.mjs" ]
CMD [ "node", "--max-semi-space-size=16", "--max-old-space-size=300", ".output/server/index.mjs" ]