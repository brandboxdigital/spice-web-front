export default {
  scrollBehavior( to, from, savedPosition ) {
    const nuxtApp = useNuxtApp()

    if (savedPosition) {
      return savedPosition;
    }

    if (to.hash != '') {
      return {
        el: to.hash,
        top: 140,
        behavior: 'smooth',
      }
    }

    // if ( from?.name === to?.name ) {
    //   return null;
    // }

    if ( from?.name?.indexOf('about_') > -1 && to?.name?.indexOf('audiostories_') > -1 ) {
      return null;
    }
    if ( from?.name?.indexOf('audiostories_') > -1 && to?.name?.indexOf('about_') > -1 ) {
      return null;
    }

    return new Promise((resolve) => {
      nuxtApp.hooks.hookOnce('page:finish', () => {
        resolve({
          top: 0,
          left: 0,
          // behavior: 'smooth'
        })
      })
    })

  }
}