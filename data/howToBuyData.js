export const list = [
    {
        header: 'Iegādājies dāvanu karti uz vietas t/c Spice informācijas centrā',
        content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
    },
    {
        header: 'Klix Pay Later',
        content: '<p>Dāvanu karti iespējams iegādāties ar aizdevumu, izmantojot AS "Citadele banka" izstrādātu risinājumu "Klix Pay Later". Pilnībā attālināta aizdevuma noformēšana ar 4 vienkāršiem soļiem:</p><ol><li>Autentificējies;</li><li>Autentificējies;</li><li>Autentificējies;</li><li>Autentificējies;</li><li>Autentificējies;</li><li>Autentificējies;</li></ol>'
    },
    {
        header: 'Kurš var iegādāties preces, izmantojot aizdevumu?',
        content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s</p>'
    },
    {
        header: 'Informācija par pirkuma apmaksu, izmantojot aizdevumu',
        content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s</p>'
    },
    {
        header: 'Spice.lv mājaslapā',
        content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s</p>'
    }
]