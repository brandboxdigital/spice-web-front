export const locations = [
    {
        title: 'Veikalos',
        subtitle: 'RIMI Hyper un citos Spice, Spice Home un Promenādes veikalos',
        background_color: '#030303', // background_color: 'var(--primary-black-100)',
        color: '#eacebf', // background_color: 'var(--primary-beige-100)',
    },
    {
        title: 'Kafejnīcās',
        subtitle: 'un restorānos',
        background_color: '#eacebf', // background_color: 'var(--primary-beige-100)',
        color: '#030303', // background_color: 'var(--primary-black-100)',
    },
    {
        title: 'Aptiekās',
        subtitle: 'un optikas veikalos',
        background_color: '#736863', // background_color: 'var(--tertiary-brown)',
        color: '#fff' //var(--primary-white-100)
    },
    {
        title: 'Day Spa',
        subtitle: 'Skaistumkopšanas salonos',
        background_color: '#736863', // background_color: 'var(--tertiary-brown)',
        color: '#fff' //var(--primary-white-100)
    },
    {
        title: 'Sporta',
        subtitle: 'Jump Space un citās izklaides vietās',
        background_color: '#030303', // background_color: 'var(--primary-black-100)',
        color: '#eacebf' //var(primary-beige-100)
    },
    {
        title: 'Par pakalpojumiem',
        subtitle: 'Pie pakalpojumu sniedzējiem',
        background_color: '#4a1c1f',  // background_color: 'var(--tertiary-noble-red)',
        color: '#eacebf' //var(primary-beige-100)
    }
]