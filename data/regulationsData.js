export const regulationsData = [
    {
        id: 'spice',
        pretext: '<p>Šie tirdzniecības centra “SPICE” (TC “SPICE” ) un pieguļošās teritorijas Lielirbes ielā 29, Rīgā lietošanas noteikumi (“Noteikumi”) ir saistoši visiem TC “SPICE” apmeklētājiem un viesiem. TC “SPICE” telpas un teritorija ir publiski pieejama vieta un attiecīgi TC apmeklētājiem un viesiem ir saistoši arī Rīgas Domes 19.06.2007 Noteikumi Nr. 80 “Sabiedriskās kārtības noteikumi Rīgā’’, kā arī visi citi saistošie normatīvie akti.</p><p>TC “SPICE” darba laiks ir sekojošs:</p><ul><li>Katru dienu no plkst. 10:00 līdz plkst. 21:00;</li><li>TC “SPICE” atrodošais RIMI strādā no plkst. 8:00 līdz 23:00;</li><li>Cits (mainīts) darbalaiks, par kuru informācija tiek publicēta vietnē www.spice.lv</li></ul><p>Kārtības uzraudzību TC “SPICE” telpās un tam pieguļošajā teritorijā veic apsardzes darbinieki (turpmāk – Apsardzes dienests), un TC “SPICE” apmeklētājiem un viesiem ir jāievēro Apsardzes dienesta darbinieku norādījumi.</p><p>TC “SPICE” telpās un pieguļošajā teritorijā darbojās video novērošana.</p>',
        items: [
            {
                header: 'TC “SPICE” telpās un pieguļošajā teritorijā ir aizliegts',
                content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
            },
            {
                header: 'Eskalatoru/ travelātora izmantošana TC “SPICE” telpās',
                content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
            },
            {
                header: 'Liftu izmantošana TC “SPICE” telpās',
                content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
            },
            {
                header: 'TC “SPICE” neuzņemas atbildību par',
                content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
            },
            {
                header: 'Apsardzes dienestam ir tiesības',
                content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
            },
            {
                header: 'Tirdzniecības centra “SPICE” Autostāvvietas lietošanas noteikumi',
                content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
            },
            {
                header: 'SIA Tirdzniecības centrs Pleskodāle neuzņemas atbildību par',
                content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
            },
            {
                header: 'Apsardzes dienestam ir tiesības',
                content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
            }
        ]
    },
    {
        id: 'spicehome',
        pretext: '<p>Šie tirdzniecības centra “SPICE HOME” (TC “SPICE HOME” ) un pieguļošās teritorijas Lielirbes ielā 29, Rīgā lietošanas noteikumi (“Noteikumi”) ir saistoši visiem TC “SPICE” apmeklētājiem un viesiem. TC “SPICE” telpas un teritorija ir publiski pieejama vieta un attiecīgi TC apmeklētājiem un viesiem ir saistoši arī Rīgas Domes 19.06.2007 Noteikumi Nr. 80 “Sabiedriskās kārtības noteikumi Rīgā’’, kā arī visi citi saistošie normatīvie akti.</p><p>TC “SPICE” darba laiks ir sekojošs:</p><ul><li>Katru dienu no plkst. 10:00 līdz plkst. 21:00;</li><li>TC “SPICE” atrodošais RIMI strādā no plkst. 8:00 līdz 23:00;</li><li>Cits (mainīts) darbalaiks, par kuru informācija tiek publicēta vietnē www.spice.lv</li></ul><p>Kārtības uzraudzību TC “SPICE” telpās un tam pieguļošajā teritorijā veic apsardzes darbinieki (turpmāk – Apsardzes dienests), un TC “SPICE” apmeklētājiem un viesiem ir jāievēro Apsardzes dienesta darbinieku norādījumi.</p><p>TC “SPICE” telpās un pieguļošajā teritorijā darbojās video novērošana.</p>',
        items: [
            {
                header: 'TC “SPICE HOME” telpās un pieguļošajā teritorijā ir aizliegts',
                content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
            },
            {
                header: 'Eskalatoru/ travelātora izmantošana TC “SPICE HOME” telpās',
                content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
            },
            {
                header: 'Liftu izmantošana TC “SPICE HOME” telpās',
                content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
            },
            {
                header: 'TC “SPICE HOME” neuzņemas atbildību par',
                content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
            },
             {
                header: 'Apsardzes dienestam ir tiesības',
                content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
            },
            {
                header: 'Tirdzniecības centra “SPICE” Autostāvvietas lietošanas noteikumi',
                content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
            },
            {
                header: 'SIA Tirdzniecības centrs Pleskodāle neuzņemas atbildību par',
                content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
            },
            {
                header: 'Apsardzes dienestam ir tiesības',
                content: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'
            }
        ]
    }
]
