export const servicesData = [
    {
      img: "/img/services/Coworking.svg",
      title: "IKEA Kopstrādes telpa",
      items: [
        {
            title: '<p><b>Spice</b>: 2.stāvā</p>',
            link: 'linkabc'
        }
      ]
    },
    {
      img: "/img/services/Davanu_karte.svg",
      title: "Dāvanu karte",
      items: [
        {
            title: '<p>Iegādājies informācijas centrā vai pasūti <a href="/">www.spice.lv</a></p>',
            link: '/'
        }
      ]
    },
    {
      img: "/img/services/Elektroautouzlade.svg",
      title: "elektroauto uzlādes stacija",
      items: [
        {
            title: '<p><b>Spice Home</b>: Eleport maksas uzlādes stacija Spice Home stāvvieta, iepretim B ieejai</p>',
            link: 'asdasd-link'
        },
        {
            title: '<p><b>Spice</b>: Elektrum maksas uzlādes stacija Spice daudzstāvu autostāvvietas 2.stāvs</p>',
            link: 'rtyryt-link'
        },
        {
            title: '<p><b>Spice Promenāde</b>: stāvvieta</p>',
            link: 'ksksks-link'
        }
      ]
    },
    {
      img: "/img/services/Pakomats.svg",
      title: "Pakomāti",
      items: [
        {
            title: '<b>Omniva</b>: ārpusē pie Spice E ieejas',
            link: ''
        },
        {
            title: '<b>DPD Paku skapis</b>: ārpusē pie Spice D ieejas',
            link: ''
        },
        {
          title: '<b>Latvijas Pasts pakomāts</b>: ārpusē pie Spice Home A ieejas (pa labi)',
          link: 'linkabc'
        }
      ]
    },
    {
      img: "/img/services/Biletes.svg",
      title: "Pasākumu biļetes",
      items: [
        {
            title: '<p>Iegādājies Spice informācijas centrā (Biļešu Serviss, Biļešu Paradīze, eKase, Aula)</p>',
            link: 'linkabc'
        }
      ]
    },
    {
      img: "/img/services/Bankomats.svg",
      title: "Bankomāts",
      items: [
        {
            title: '<b>Spice</b>: pie eskalatora pretī ZARA',
            link: ''
        },
        {
            title: '<b>Spice Home</b>: pie eskalatoriem, pretī TET',
            link: 'linkabc'
        }
      ]
    },
    {
      img: "/img/services/Playground.svg",
      title: "bērnu rotaļlaukums",
      items: [
        {
            title: '<b>Spice</b>: ārpusē, pretī B ieejai',
            link: 'linkabc'
        },
        {
            title: '<b>Spice Home</b>: bezmaksas rotaļlaukums 2.stāvā',
            link: 'linkabc'
        }
      ]
    },
    {
      img: "/img/services/Ziedojumi.svg",
      title: "Ziedošanas siena",
      items: [
        {
            title: 'Pie Spice B ieejas',
            link: 'linkabc'
        }
      ]
    },
    {
      img: "/img/services/Water.svg",
      title: "Ūdens ņemšanas vieta",
      items: [
        {
            title: '<b>Spice</b>: Palmu alejā 1.stāvā',
            link: 'linkabc'
        }
      ]
    },
    {
      img: "/img/services/Makulatura.svg",
      title: "makulatūras nodošanas punkts",
      items: [
        {
            title: 'Aiz Spice Home no Jaunmoku ielas puses',
            link: 'linkabc'
        }
      ]
    },
    {
      img: "/img/services/Mobila_uzlade.svg",
      title: "Mobilo tālruņu elektrouzlāde",
      items: [
        {
            title: 'Skapīši uzlādei atrodas Spice informācijas centrā',
            link: 'linkabc'
        }
      ]
    },
    {
      img: "/img/services/Tekstils.svg",
      title: "tekstila šķirošanas konteineri",
      items: [
        {
            title: '<b>Pie Spice rampas</b>: daudzstāvu autostāvvietas galā',
            link: 'linkabc'
        },
        {
            title: '<b>Spice Home</b>: pie B ieejas',
            link: 'linkabc'
        }
      ]
    },
    {
      img: "/img/services/Autostavieta.svg",
      title: "Virszemes un daudzstāvu atostāvvieta",
      items: [
        {
            title: 'Atvērta katru dienu no 10:00 līdz 23:00',
            link: 'linkabc'
        }
      ]
    },
    {
      img: "/img/services/Taxi.svg",
      title: "Taksometru stāvvieta",
      items: [
        {
            title: 'Pie Spice D ieejas',
            link: 'linkabc'
        }
      ]
    },
    {
      img: "/img/services/BikeParking.svg",
      title: "Riteņu un elektroskūteru novietnes",
      items: [
        {
            title: 'Šeit ir teksts, kur var atrast šo',
            link: 'linkabc'
        }
      ]
    }
  ]