const recursevlyFlatten = (obj, path = []) => {
    if (typeof obj !== 'object') return { [path.join('.')]: obj }
    return Object.entries(obj).reduce((product, [key, value]) => {
        return { ...product, ...recursevlyFlatten(value, [...path, key]) }
    }, {})
}

export default defineI18nLocale( (locale) => {

    const res = $fetch(`/api/v1/content/static-strings`, {
        headers: {
            'accept-language': locale
        }
    }).then((res) => {
        return res?.data || {}
    })

    return res
})