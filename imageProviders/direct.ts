import type { ProviderGetImage } from "@nuxt/image"

export const getImage: ProviderGetImage = (src, { modifiers = {}, baseURL } = {}) => {
  return {
    url: src
  }
}
