import { joinURL } from "ufo"
import type { ProviderGetImage } from "@nuxt/image"

export const getImage: ProviderGetImage = (src, { modifiers = {}, baseURL } = {}) => {
  if (!baseURL) {
    baseURL = useRuntimeConfig().public.IMAGOR_SERVER
  }
  if (src.startsWith("/")) {
    return {
      url: src,
    }
  }

  const urlWidthoutProtocol = src.substring(src.indexOf(`://`) + 3)

  if (urlWidthoutProtocol.endsWith(".svg")) {
    return {
      url: joinURL("https://", urlWidthoutProtocol),
    }
  }

  const width = modifiers.width || ""
  const height = modifiers.height || ""
  const quality = modifiers.quality || 80
  const format = modifiers.format || "jpg"

  const size = width || height ? `${width}x${height}` : ""
  const request = `${size}/filters:quality(${quality}):format(${format})/` + urlWidthoutProtocol

  return {
    url: joinURL(baseURL, "unsafe", request),
  }
}
