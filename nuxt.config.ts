// https://nuxt.com/docs/api/configuration/nuxt-config

// import Nora from '@primevue/themes/nora';

const GTM = 'GTM-MHK8P46';

export default defineNuxtConfig({
  runtimeConfig: { // rewrite this with .env
    BACKEND: process.env.API_ROUTE || "https://cms.spice.lv",

    public: {
      BACKEND: process.env.API_ROUTE || "https://cms.spice.lv",
      IMAGOR_SERVER: process.env.IMAGOR_SERVER,
      gtm: {
        id: GTM,
      },
    },
  },

  site: {
    url: 'https://spice.lv',
    name: 'SPICE'
  },

  app: {
    pageTransition: false,

    head: {
      charset: "utf-8",
      title: 'Tirdzniecības centrs SPICE',
      viewport: "width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0",
      htmlAttrs: {
        lang: 'lv'
      },

      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: 'TC Spice piedāvā iepirkšanos vairāk nekā 200 modes un dzīvesstila veikalos, tostarp, tādu unikālu zīmolu kā Sandro, Maje, Hugo, Michael Kors, Marella, iBlues, Weekend Max Mara u.c.' },
        { name: 'robots', content: 'index, follow' },
      ],

      link: [
        { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
        { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
        { rel: 'preconnect', href: 'https://fonts.gstatic.com', crossorigin: "" },
        { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Lexend+Deca:wght@100..900&family=Lexend+Giga:wght@100..900&display=swap' },
      ]
    }
  },

  css: ["~/assets/scss/main.scss"],
  devtools: {
    enabled: true,

    timeline: {
      enabled: true,
    },
  },

  modules: [
    "@nuxt/image",
    "@nuxtjs/sitemap",
    "@pinia/nuxt",
    "@nuxtjs/i18n",
    '@primevue/nuxt-module',
    '@nuxtjs/tailwindcss',
    'dayjs-nuxt',
    'nuxt-swiper',
    '@nuxtjs/leaflet',
    'nuxt-svgo',

    //https://nuxt.com/modules/nuxt-gtm
    '@zadigetvoltaire/nuxt-gtm'
  ],

  future: {
    compatibilityVersion: 3,
  },

  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          silenceDeprecations: [
            'import',
            'legacy-js-api',
            'mixed-decls',
          ],
          additionalData: '@import "@/assets/scss/import.scss";',
        },
      },
    },
  },

  i18n: {
    locales: [
      { code: 'en', language: 'en-US', file: 'lazy.js', },
      { code: 'lv', language: 'lv-LV', file: 'lazy.js', },
    ],

    defaultLocale: 'lv',
    lazy: true,
    langDir: 'lang',

    strategy: 'prefix',

    // experimental: {
    //   localeDetector: './localeDetector.ts'
    // },

    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: 'i18n_redirected',
      redirectOn: 'root',  // recommended
    },

    compilation: {
      strictMessage: false,
      escapeHtml: false,
    }
  },

  image: {
    // domains: ['cms.spice.lv'],
    // provider: process.env.IMAGE_PROVIDER || 'direct',
    provider: 'direct',
    providers: {
      direct: {
        name: 'direct',
        provider: '~/imageProviders/direct.ts',
      },
      imagor: {
        name: 'imagor',
        provider: '~/imageProviders/imagor.ts',
      },
    }
  },

  sitemap: {
    sources: [
       '/api/__sitemap__/urls',
    ]
  },

  primevue: {
    autoImport: true,
    options: {
      unstyled: false,
      theme: {
        // preset: Nora,
        options: {
            // prefix: 'spice',
            // darkModeSelector: 'system',
            cssLayer: 'primevue',
            // cssLayer: 'tailwind-utilities, primevue, tailwind-base, primeui, main',
          }
      }
    },
    // components: {
    //     include: ['Button', 'InputText', 'Select', 'InputNumber', 'Dialog', 'RadioButton', 'AutoComplete', 'Toast', 'TextArea'],
    // },
    // directives: {
    //     include: ['Tooltip', 'Dialog']
    // },
    // composables: {
    //     include: ['useStyle', 'useDialog']
    // },
  },

  dayjs: {
    locales: ['en', 'lv'],
    plugins: ['relativeTime', 'utc', 'timezone'],
    defaultLocale: 'en',
    defaultTimezone: 'Europe/Riga',
  },

  swiper: {
    // modules: ['navigation', 'pagination', 'scrollbar', 'zoom'],
    modules: ['navigation'],
  },

  svgo: {
    autoImportPath: false,
    global: false,
  },

  nitro: {
    routeRules: {
      '/api/v1/**': {
        cors: true,
        proxy: `${process.env.API_ROUTE || 'https://cms.spice.lv'}/api/v1/**`,
        // proxy: 'http://localhost:5000/api/v1/**',
        cache: {
          maxAge: 60, // 60 seconds cache
        }
      },
      '/api/v2/**': {
        cors: true,
        proxy: `${process.env.API_ROUTE || 'https://cms.spice.lv'}/api/v2/**`,
        // proxy: 'http://localhost:5000/api/v1/**',
        cache: {
          maxAge: 0, // 60 seconds cache
        }
      },
      '/api/v3/**': {
        cors: true,
        proxy: `${process.env.API_ROUTE || 'https://cms.spice.lv'}/api/v3/**`,
        // proxy: 'http://localhost:5000/api/v1/**',
        cache: {
          maxAge: 60, // 60 seconds cache
        }
      },

      "/media/**": {
        cors: true,
        proxy: `${process.env.API_ROUTE || 'https://cms.spice.lv'}/storage/app/media/**`,
      },

      "/storage/app/media/**": {
        cors: true,
        proxy: `${process.env.API_ROUTE || 'https://cms.spice.lv'}/storage/app/media/**`,
      },

    },

    compressPublicAssets: {
      gzip: true,
      brotli: true,
    },
  },

  gtm: {
    enabled: true,
    id: GTM,
  },

  // https://www.spice.lv/lv/gift-card/result?bank=cancel
  routeRules: {
    '/lv/gift-card/result': { redirect: '/lv/davanu-kartes' },
    '/en/gift-card/result': { redirect: '/lv/gift-cards' },
    // '/contact.html': { redirect: '/contact' },
    // '/external-route': { redirect: 'https://example.com' },
  },

  compatibilityDate: '2024-04-03',
})