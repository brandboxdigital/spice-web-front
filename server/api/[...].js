export default defineEventHandler(async (event) => {
    const { BACKEND } = useRuntimeConfig()
    
    const target = BACKEND + event.path

    return proxyRequest(event, target)
})