import { asSitemapUrl, defineSitemapEventHandler } from "#imports"

const siteLanguages = ["lv", "en"]

const routes = {
  audiostories: { lv: "audiostasti", en: "audiostories" },
  discounts: { lv: "akcijas", en: "offers" },
  internal: { lv: "apmekletaju-ieksejas-kartibas-noteikumi", en: "rules-of-internal-procedures-for-visitors" },
  news: { lv: "jaunumi", en: "news" },
  shops: { lv: "veikali", en: "shops" },
}

export default defineSitemapEventHandler(async (event) => {
  const dataLocalised = siteLanguages.map(async (locale) => {
    const header = {
      headers: {
        "Accept-Language": locale,
      },
    }
    return {
      ...(await $fetch<ReturnType<typeof asSitemapUrl>>("/api/v3/init", header).then((res) => res.data)),
      ...(await $fetch<ReturnType<typeof asSitemapUrl>>("/api/v1/posts/news", header).then((res) => ({
        news: res.data
      }))),
      ...(await $fetch<ReturnType<typeof asSitemapUrl>>("/api/v1/shop/shops", header).then((res) => ({
        shops: res.data
      }))),
      locale,
    }
  })  

  const data = await Promise.all(dataLocalised)

  // Read audiostories
  const audioStories = siteLanguages.flatMap((locale) =>
    data
      .filter((d) => d.locale === locale)
      .map((d) =>
        d.about.audioStories.map((item) =>
          asSitemapUrl({
            loc: `/${locale}/${routes.audiostories[locale as "lv" | "en"]}/${item.slug}`,
          })
        )
      )
      .flat()
  )

  // Offers
  const offersMain = siteLanguages.flatMap((locale) =>
    asSitemapUrl({
      loc: `/${locale}/${routes.discounts[locale as "lv" | "en"]}`,
    })
  )

  // Offers by category
  const offersByCategory = siteLanguages.flatMap((locale) =>
    data
      .filter((d) => d.locale === locale)
      .map((d) =>
        Object.values(d.categories).map((category) =>
          asSitemapUrl({
            loc: `/${locale}/${routes.discounts[locale as "lv" | "en"]}/${category.slug}`,
          })
        )
      )
      .flat()
  )

  // Internal procedures
  const internalsMain = siteLanguages.flatMap((locale) => [
    asSitemapUrl({
      loc: `/${locale}/${routes.internal[locale as "lv" | "en"]}/spice`,
    }),
    asSitemapUrl({
      loc: `/${locale}/${routes.internal[locale as "lv" | "en"]}/spicehome`,
    }),
  ])

  // News
  const news = siteLanguages.flatMap((locale) =>
    data
      .filter((d) => d.locale === locale)
      .map((d) =>
        d.news.map((item) =>
          asSitemapUrl({
            loc: `/${locale}/${routes.news[locale as "lv" | "en"]}/${item.slug}`,
          })
        )
      )
      .flat()
  )

  // Shops
  const shops = siteLanguages.flatMap((locale) =>
    data
      .filter((d) => d.locale === locale)
      .map((d) =>
        d.shops.map((item) =>
          asSitemapUrl({
            loc: `/${locale}/${routes.shops[locale as "lv" | "en"]}/${item.categories[0].slug}/${item.slug}`,
          })
        )
      )
      .flat()
  )

  return [
    ...audioStories,
    ...offersMain,
    ...offersByCategory,
    ...internalsMain,
    ...news,
    ...shops,
  ]

  return []
})
