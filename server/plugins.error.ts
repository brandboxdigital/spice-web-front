/**
 * @url https://stackoverflow.com/questions/75650321/nuxt-3-404-page-not-found-error-and-h3error-page-not-found
 */

import type { H3Error, H3Event } from 'h3'

export default defineNitroPlugin((nitroApp) => {
    const h3OnError = nitroApp.h3App.options.onError;
    nitroApp.h3App.options.onError = (error : Error, event : H3Event) => {
        let h3error : H3Error = error as H3Error;
        console.log('?', h3error);

        if(h3error?.statusCode === 404)
        {
            h3error.unhandled = false;
        }

        if(h3OnError !== undefined)
        {
            return h3OnError(error, event);
        }
        return;
    };
})
