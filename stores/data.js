export const useDataStore = defineStore('data', () => {

    const nuxtApp = useNuxtApp()

    // const config     = useRuntimeConfig()
    // const API_DOMAIN = config?.public?.BACKEND || 'https://cms.spice.lv';

    const scrollbarWidth    = ref(0);

    const categories        = ref(null);
    const buildings         = ref(null);
    const shops             = ref(null);
    const spiceUniqueShops  = ref(null);
    const sales             = ref(null);
    const audioStories      = ref(null);
    const aboutContent      = ref(null);
    const news              = ref(null);
    const services          = ref(null);
    const frontpageservices = ref(null);
    const giftcardinfo      = ref(null);
    const sustainablility   = ref(null);
    const kids              = ref(null);
    const workhours         = ref(null);
    const homepageBanner    = ref(null);
    const pages             = ref(null);

    const fetchData = async (locale) => {

        const { data: initData, error, refresh } = await useFetch(`/api/v3/init`, {
            key: `initData_${locale}`,
            headers: { 'accept-language': locale },

            getCachedData: (key) => {
                // Check if the data is already cached in the Nuxt payload
                if (nuxtApp.isHydrating && nuxtApp.payload.data[key]) {
                    return nuxtApp.payload.data[key]
                }

                // Check if the data is already cached in the static data
                if (nuxtApp.static.data[key]) {
                    return nuxtApp.static.data[key]
                }

                return null
            },
        })

        categories.value         = initData.value?.data?.categories || null
        buildings.value          = initData.value?.data?.buildings || null

        spiceUniqueShops.value   = initData.value?.data?.spiceuniqueshops || null

        services.value           = initData.value?.data?.services || null
        frontpageservices.value  = initData.value?.data?.frontpageservices || null

        giftcardinfo.value       = initData.value?.data?.giftcardinfo || null

        sustainablility.value    = initData.value?.data?.sustainablility || null

        kids.value               = initData.value?.data?.kids || null
        workhours.value          = initData.value?.data?.workhours || null

        audioStories.value       = initData.value?.data?.about?.audioStories || null
        aboutContent.value       = initData.value?.data?.about?.content || null

        homepageBanner.value     = initData.value?.data?.banner || null

        pages.value              = initData.value?.data?.pages || null

        fetchShops(locale);
        fetchSales(locale);
        fetchNews(locale);
    }

    const getFilteredBuildingsForMap = () => {
        return buildings.value?.filter(theme => theme.slug !== 'spicepromenade') || null
    }

    const getSalesByShopId = (shopId) => {
        return sales.value?.filter(sale => sale.shop_id === shopId) || null
    }

    const mesureScrollBarWidth = () => {
        if (window && document) {
            // Create the measurement node
            const scrollDiv = document.createElement("div");
            scrollDiv.className = "scrollbar-measure";
            document.body.appendChild(scrollDiv);

            // Get the scrollbar width
            scrollbarWidth.value = scrollDiv.offsetWidth - scrollDiv.clientWidth;

            // Delete the DIV
            document.body.removeChild(scrollDiv);

            try {
                document.documentElement.style.setProperty(`--scrollbar-size`, `${scrollbarWidth.value}px`);
            } catch (error) {}
        }
    }

    const fetchShops = (locale) => {
        return useFetch(`/api/v1/shop/shops`, {
            key: `shopsData_${locale}`,
            headers: { 'accept-language': locale },
            getCachedData: (key) => {
                // Check if the data is already cached in the Nuxt payload
                if (nuxtApp.isHydrating && nuxtApp.payload.data[key]) {
                    return nuxtApp.payload.data[key]
                }

                if (shops.value) {
                    return shops.value
                }

                return null
            },
        }).then((response) => {

            if (response?.data?.value) {
                shops.value = response?.data?.value?.data || null
            }
        });
    }

    const fetchSales = (locale) => {

        return useFetch(`/api/v1/shop/sales`, {
            key: `salesData_${locale}`,
            headers: { 'accept-language': locale },
            getCachedData: (key) => {
                // Check if the data is already cached in the Nuxt payload
                if (nuxtApp.isHydrating && nuxtApp.payload.data[key]) {
                    return nuxtApp.payload.data[key]
                }

                if (sales.value) {
                    return sales.value
                }

                return null
            },
        }).then((response) => {
            if (response?.data?.value) {
                sales.value = response?.data?.value?.data || null
            }
        });
    }

    const fetchNews = (locale) => {

        return useFetch(`/api/v1/posts/news`, {
            key: `newsData_${locale}`,
            headers: { 'accept-language': locale },
            getCachedData: (key) => {
                // Check if the data is already cached in the Nuxt payload
                if (nuxtApp.isHydrating && nuxtApp.payload.data[key]) {
                    return nuxtApp.payload.data[key]
                }

                if (news.value) {
                    return news.value
                }

                return null
            },
        }).then((response) => {
            if (response?.data?.value) {
                news.value = response?.data?.value?.data || null
            }
        });
    }

    return {

        categories,
        buildings,
        shops,
        sales,
        audioStories,
        aboutContent,
        news,
        services,
        frontpageservices,
        giftcardinfo,

        sustainablility,


        kids,
        workhours,
        homepageBanner,

        spiceUniqueShops,

        fetchData,

        fetchShops,
        fetchSales,
        fetchNews,

        getFilteredBuildingsForMap,
        getSalesByShopId,

        mesureScrollBarWidth,
    }
})