export const useI18nStore = defineStore('i18n', {
  state: () => ({
    routeParams: {},
  }),
  actions: {
    setRouteParams(params) {
      this.routeParams = params;
    },
  },
});