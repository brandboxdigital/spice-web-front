export const useOpentimesStore = defineStore('opentimes', {
  state: () => ({
    spice: null,
    spicehome: null,
    loading: true
  }),
  mutations: () => ({
    setSpice(state, value) {
      state.spice = value;
    },

    setSpiceHome(state, value) {
      state.spicehome = value;
    },
  }),
  actions: () => ({
    async getData({ commit }) {
        const times = await this.$axios.$get('https://cms.spice.lv/v1/content/worktimes');
        commit('setSpice',     times.data.spice);
        commit('setSpiceHome', times.data.spicehome);
      },
  })
});