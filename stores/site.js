export const useSiteStore = defineStore('site', () => {
    const example = ref(null);
    const exampleFn = () => { example.value = 'a' };

    return {
        example,
        exampleFn,
    };
})