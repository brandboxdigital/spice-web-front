/** @type {import('tailwindcss').Config} */
export default {
  content: [],
  theme: {
    container: {
      center: true,
      padding: '0.75rem',
    },
    extend: {},
  },
  plugins: [],
}
